#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <sys/time.h>

// 1 = sqrt(pow(y,2)+pow(x,2))
// y = sqrt(1-pow(x,2))

#define ITERS 1000000000

int main(){
    long long start_ts;
	long long stop_ts;
	long long elapsed_time;
	long lElapsedTime;
	struct timeval ts;
    double y;
    double step = 1.0/ITERS;
    double x;
    double suma = 0.0;
    long i;
    printf("step = %.10lf\n", step);

    gettimeofday(&ts, NULL);
	start_ts = ts.tv_sec; // Tiempo inicial

    for(i=0; i<ITERS;i++){
        x = (i + 0.5)*step;
        y = sqrt(1-x*x);
        suma += step*y;
        //printf("x = %.4lf, y = %.4lf, sum = %.4lf\n", x, y, suma);
    }
    suma *= 4;
    
    gettimeofday(&ts, NULL);
    stop_ts = ts.tv_sec; // Tiempo final
	elapsed_time = stop_ts - start_ts;
    printf("Resultado = %.16lf\n",suma);
    printf("------------------------------\n");
	printf("TIEMPO TOTAL, %lld segundos\n",elapsed_time);
}