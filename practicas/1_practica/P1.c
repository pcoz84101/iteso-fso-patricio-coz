#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <sys/time.h>
#include <pthread.h>

#define ITERS 1000000000
#define NUM_THREADS 4 // Puedes ajustar esto al número de núcleos de tu procesador

pthread_mutex_t mutex;

double step;
double suma = 0.0;

void *calculate_pi(void *thread_id) {
    long tid = (long)thread_id;
    double partial_sum = 0.0;
    
    for (long i = tid; i < ITERS; i += NUM_THREADS) {
        double x = (i + 0.5) * step;
        double y = sqrt(1 - x * x);
        partial_sum += step * y;
    }
    
    // Agregar la contribución parcial a la suma global
    pthread_mutex_lock(&mutex);
    suma += partial_sum;
    pthread_mutex_unlock(&mutex);
    
    pthread_exit(NULL);
}

int main() {
    long long start_ts;
    long long stop_ts;
    long long elapsed_time;
    struct timeval ts;
    double resultado;
    pthread_t threads[NUM_THREADS];
    //pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    pthread_mutex_init(&mutex,NULL);
    step = 1.0 / ITERS;

    gettimeofday(&ts, NULL);
    start_ts = ts.tv_sec; // Tiempo inicial

    for (long i = 0; i < NUM_THREADS; i++) {
        pthread_create(&threads[i], NULL, calculate_pi, (void *)i);
    }

    for (long i = 0; i < NUM_THREADS; i++) {
        pthread_join(threads[i], NULL);
    }

    gettimeofday(&ts, NULL);
    stop_ts = ts.tv_sec; // Tiempo final
    elapsed_time = stop_ts - start_ts;
    
    resultado = suma * 4;

    printf("Resultado = %.16lf\n", resultado);
    printf("------------------------------\n");
    printf("TIEMPO TOTAL, %lld segundos\n", elapsed_time);

    return 0;
}
