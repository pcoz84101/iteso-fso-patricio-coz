/*
Para compilar incluir la librería m (matemáticas)
Ejemplo:
gcc -o mercator mercator.c -lm
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/shm.h>

#include "semaphoresarr.h"

#define NPROCS 4
#define SERIES_MEMBER_COUNT 200000

#define DEBUG   // Definir para mensajes de debug

SEM_ID semarr;
enum {SLAVE_START_ALL, SLAVE_FINISH, SLAVE_ALL_FINISH};  // Semáforos 0, 1 y 2

double *sums;
int *slaves_finished;
double x = 1.0;
double *res;

double get_member(int n, double x)
{
    int i;
    double numerator = 1;

    for(i=0; i<n; i++ )
        numerator = numerator*x;
    
    if (n % 2 == 0)
        return ( - numerator / n );
    else
        return numerator/n;
}

void proc(int proc_num)
{
    int i;

    #ifdef DEBUG
    printf("Proceso esclavo %d esperando a maestro\n", proc_num);
    fflush(stdout);
    #endif

    semwait(semarr,SLAVE_START_ALL);
    semsignal(semarr,SLAVE_START_ALL);

    #ifdef DEBUG
    printf("Proceso esclavo %d ejecutando su tarea\n", proc_num);
    fflush(stdout);
    #endif

    sums[proc_num] = 0;
    
    for(i=proc_num; i<SERIES_MEMBER_COUNT;i+=NPROCS)
        sums[proc_num] += get_member(i+1, x);
    
    #ifdef DEBUG
    printf("Proceso esclavo %d termino su tarea\n", proc_num);
    fflush(stdout);
    #endif

    semwait(semarr,SLAVE_FINISH);
    (*slaves_finished)++;
    if(*slaves_finished == NPROCS)
        semsignal(semarr,SLAVE_ALL_FINISH);
    semsignal(semarr,SLAVE_FINISH);

    exit(0);
}

void master_proc()
{
    int i;
    //int busy_slaves = NPROCS;

    sleep(1);

    #ifdef DEBUG
    printf("Proceso maestro desbloquea a todos los esclavos\n");
    fflush(stdout);
    #endif

    *slaves_finished = 0;
    semsignal(semarr,SLAVE_START_ALL);

    #ifdef DEBUG
    printf("Proceso maestro espera a que los esclavos terminen su tarea\n");
    fflush(stdout);
    #endif

    //while (busy_slaves > 0)
    //{
        semwait(semarr,SLAVE_ALL_FINISH);
    //    busy_slaves--;
    //}

    #ifdef DEBUG
    printf("Proceso maestro enterado de que sus esclavos terminaron sus tareas\n");
    fflush(stdout);
    #endif

    *res = 0;
    for(i=0; i<NPROCS; i++)
        *res += sums[i];

    exit(0);
}

int main()
{
    int *threadIdPtr;
    long long start_ts;
    long long stop_ts;
    long long elapsed_time;
    long lElapsedTime;
    struct timeval ts;
    int i;
    int p;
    int shmid;
    void *shmstart;

    shmid = shmget(0x1234,NPROCS*sizeof(double) + sizeof(int),0666|IPC_CREAT);
    shmstart = shmat(shmid,NULL,0);

    sums = shmstart;
    slaves_finished = shmstart + NPROCS * sizeof(double);
    res = shmstart + NPROCS * sizeof(double) + sizeof(int);

    // Creación del arreglo de semáforos
    semarr = createsemarray((key_t) 0x4008, 3);

    initsem(semarr, SLAVE_START_ALL, 0);
    initsem(semarr, SLAVE_FINISH, 1);
    initsem(semarr, SLAVE_ALL_FINISH, 0);

    #ifdef DEBUG
    printf("Semáforos creados\n");
    #endif

    printf("El recuento de ln(1 + x) miembros de la serie de Mercator es %d\n",SERIES_MEMBER_COUNT);
    printf("El valor del argumento x es %f\n", (double)x);

    gettimeofday(&ts, NULL);
    start_ts = ts.tv_sec; // Tiempo inicial

    for(i=0; i<NPROCS;i++)
    {
        p = fork();
        if(p==0)
            proc(i);
    }

    p = fork();
    if(p==0)
        master_proc();

    for(int i=0;i<NPROCS+1;i++)
        wait(NULL);

    gettimeofday(&ts, NULL);
    stop_ts = ts.tv_sec; // Tiempo final
    elapsed_time = stop_ts - start_ts;

    printf("Tiempo = %lld segundos\n", elapsed_time);
    printf("El resultado es %10.8f\n", *res);
    printf("Llamando a la función ln(1 + %f) = %10.8f\n",x, log(1+x));
    
    erasesem(semarr);

    shmdt(shmstart);
    shmctl(shmid,IPC_RMID,NULL);
}
