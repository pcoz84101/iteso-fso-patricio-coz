#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(){
	int p, i;
	p=fork();
	for(i=0;i<10;i++){
		if(!p){
			printf("Soy el hijo\n");
			if(i == 9)
				exit(0);
		}
		else
			printf("Soy el padre\n");
		sleep(1);
	}
	wait(NULL);
	printf("Mi proceso hijo ya ha terminado\n");
	return 0;
}
