#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

void recursive_tree(int level, int current) {
    if (level == 0) {
        printf("%d\n", current);
        return;
    }
    
    printf("%d\n", current);
    int pid = fork();
    
    if (pid == 0) {
        recursive_tree(level - 1, current);
        exit(0);
    } else {
        wait(NULL);
    }
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Usage: %s <n>\n", argv[0]);
        return 1;
    }
    
    int n = atoi(argv[1]);
    recursive_tree(n, 0);
    
    return 0;
}
