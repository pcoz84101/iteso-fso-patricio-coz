#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>

#define MAX_NAME_LENGTH 100

int main() {
    char program_name[MAX_NAME_LENGTH];
    
    while (1) {
        printf("Ingrese el nombre del programa a ejecutar (o 'exit' para salir): ");
        scanf("%s", program_name);
        
        if (strcmp(program_name, "exit") == 0) {
            printf("Saliendo del programa\n");
            break;
        }
        
        int pid = fork();
        
        if (pid == 0) {
            execl(program_name, program_name, NULL);
            printf("Error en la ejecución del programa\n");
            exit(1);
        } else {
            wait(NULL);
        }
    }
    
    return 0;
}
