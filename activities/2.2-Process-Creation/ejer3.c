#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc,char *argv[]){
	int ppid, pid;
	unsigned int i, n, number;
	ppid = getpid();	// Encontrar el pid del padre (ppid = parent pid)
	n = atoi(argv[1]);
	for(i=1; i<n+1; i++){
		if(ppid == getpid())	//Si eres el padre, crea un hijo
		if(!fork())			//Si eres el nuevo hijo, guarda tu número en number
		number = i;
	}
	if(ppid != getpid()){		//Si no eres el padre
		printf("Proceso hijo %d\n", number);
		exit(0);
	}
	i=1;
	while(i<n){					//Esperar a que tus n hijos mueran
		wait(NULL);
		i++;
	}
	printf("Fin\n");
	return 0;
}
