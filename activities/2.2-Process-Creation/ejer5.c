#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>
#define MAX 60

void get_name(char * name){
	printf("Ingrese el nombre del programa a ejecutar: ");
	scanf("%59s", name); // Lee máximo 59 caracteres para dejar al menos un '\0' al final
	if(strcmp(name, "exit") != 0)
		printf("Ejecutando %s\n", name);
	else
		printf("Hasta luego\n");
}

int main(){
	char name[MAX] = {0};
	get_name(name);
	while(strcmp(name, "exit") != 0){	//Mientras name no contenga "exit\0"
		if(!fork()){ //Crea un hijo y el hijo hace lo siguiente
			execl(name, name, NULL); //Ejecuta el programa y el proceso hijo muere 
			printf("Error en al ejecución del programa\n"); //Si el programa no se ejecutó muestra error y finaliza el proceso hijo
			exit(0);
		}
		wait(NULL); //El padre espera a que el proceso hijo muera
		get_name(name);
	}
	return 0;
}
