#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(){
    if(!fork){
        sleep(1);   //El hijo dura 1 segundo
        exit(0);
    }
    else
        sleep(20);  //El padre dura 20 segundos
    return 0;
}