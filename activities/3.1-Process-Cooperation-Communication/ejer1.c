#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

int main() {
    pid_t pid = fork();

    if (pid < 0) {
        fprintf(stderr, "Error al crear el proceso hijo\n");
        exit(1);
    } else if (pid == 0) {
        // Proceso hijo
        sleep(1);
        printf("Proceso hijo (%d) matando al padre (%d)\n", getpid(), getppid());
        kill(getppid(), SIGTERM);
    } else {
        // Proceso padre
        printf("Proceso padre (%d)\n", getpid());
        while (1) {
            // Do Nothing
        }
    }

    return 0;
}
