#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#define HIJOS 2
#define NIETOS 3

void funcion_nieto(int numero_nieto, int numero_hijo){
    printf("Hola, soy el nieto #%d del hijo #%d\n", numero_nieto, numero_hijo);
    sleep(20);
}

void funcion_hijo(int numero_hijo){
    int i;
    printf("Hola, soy el hijo #%d\n", numero_hijo);
    for(i=0; i<NIETOS; i++)
        if(!fork())
            funcion_nieto(i+1, numero_hijo);
    sleep(20);
}

int main(){
    int i;
    /*El padre crea 2 hijos*/
    for(i=0;i<HIJOS;i++)
        if(!fork())
            funcion_hijo(i+1); //El hijo hace la función de hijo y se le asigna un número
    sleep(5);
    /*Mata a todos sus hijos y nietos*/
    kill(0,9);                  //Le envía a todas las PID de su grupo (número 0) la señal SIGKILL (número 9)
    printf("Maté a mis hijos >:)\n");
    return 0;
}