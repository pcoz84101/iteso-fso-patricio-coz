#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main() {
    pid_t pid = fork();

    if (pid < 0) {
        fprintf(stderr, "Error al crear el proceso hijo\n");
        exit(1);
    } else if (pid == 0) {
        // Proceso hijo
        sleep(20); // El hijo dura 20 segundos
        printf("Hijo (%d) terminado\n", getpid());
    } else {
        // Proceso padre
        printf("Proceso padre (%d)\n", getpid());
        sleep(1); // El padre dura 1 segundo
        printf("Proceso padre (%d) terminado\n", getpid());
    }

    return 0;
}
