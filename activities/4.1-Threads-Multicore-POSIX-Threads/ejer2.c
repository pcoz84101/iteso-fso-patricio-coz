#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>
#include <pthread.h>

#define ITERS 1000000000

typedef struct{
	int offset;
	int iters;
	float x;
}thread_args;

pthread_mutex_t lock;

double sum = 0.0;

void* tfunc(void* args){
	int n;
	double sumlocal = 0.0;
	thread_args * argumentos = (thread_args*) args;
	int inicio = argumentos->offset;
	int fin = inicio + argumentos->iters;
	float x = argumentos->x;

	//pthread_mutex_lock(&lock);
	//printf("Valor de x: %.4f; inicio = %d -> fin = %d\n", x, inicio, fin - 1);
	//pthread_mutex_unlock(&lock);

	for(n=inicio;n<fin;n++)
		sumlocal = sumlocal + pow(-1,n+1) * pow(x,n) / n;

	pthread_mutex_lock(&lock);
	sum += sumlocal;
	pthread_mutex_unlock(&lock);
}

int main()
{
	long long start_ts;
	long long stop_ts;
	long long elapsed_time;
	long lElapsedTime;
	struct timeval ts;
	int i, n, N_CPUs, offset = 1, iters, itersXthread, resto;
	float x;
	pthread_t * tid;
	thread_args * argumentos;

	pthread_mutex_init(&lock,NULL);

	printf("Calcular el logaritmo natural de 1+x\n");
	printf("Dame el valor de x :");
	scanf("%f",&x);

	do{
		printf("Ingrese la cantidad de nucleos de su procesador: ");
		fflush(stdin);
		scanf("%d", &N_CPUs);
	} while(N_CPUs <= 0);

	tid = malloc(N_CPUs*sizeof(pthread_t));
	argumentos = malloc(N_CPUs*sizeof(thread_args));

	itersXthread = ITERS/N_CPUs;
	resto = ITERS%N_CPUs;
	for(i=0; i<N_CPUs; i++){
		printf("inicio = %d -> ", offset);
		argumentos[i].offset = offset;
		iters = itersXthread;
		if(resto != 0){
			iters++;
			resto--;
		}
		printf("fin = %d\n", iters + offset - 1);
		argumentos[i].iters = iters;
		argumentos[i].x = x;
		offset += iters;
	}
	
	gettimeofday(&ts, NULL);
	start_ts = ts.tv_sec; // Tiempo inicial

	//for(n=1;n<ITERS;n++)
	//	sum = sum + pow(-1,n+1) * pow(x,n) / n;

	for(i=0;i<N_CPUs;i++)
		pthread_create(&tid[i],NULL,tfunc,&argumentos[i]);
    
    for(i=0;i<N_CPUs;i++)
        pthread_join(tid[i],NULL);

	pthread_mutex_destroy(&lock);
	
	gettimeofday(&ts, NULL);
	stop_ts = ts.tv_sec; // Tiempo final

	elapsed_time = stop_ts - start_ts;
	printf("Resultado = %.10lf\n",sum);
	printf("------------------------------\n");
	printf("TIEMPO TOTAL, %lld segundos\n",elapsed_time);
}
