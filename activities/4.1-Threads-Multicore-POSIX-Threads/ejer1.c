#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>
#include <pthread.h>

#define SIZE 4000

#define INICIAL 900000000
#define FINAL 1000000000

typedef struct{
	int offset;
	int filas;
}thread_args;

int mat[SIZE][SIZE];

void initmat(int mat[][SIZE]);
void printnonzeroes(int mat[SIZE][SIZE]);
int isprime(int n);

void* tfunc(void* args){
	int i,j;
	thread_args * argumentos = (thread_args*) args;
	int inicio = argumentos->offset;
	int fin = inicio + argumentos->filas;

	for(i=inicio;i<fin;i++)
		for(j=0;j<SIZE;j++)
			if(!isprime(mat[i][j]))
			   mat[i][j]=0;

}

int main()
{
	long long start_ts;
	long long stop_ts;
	long long elapsed_time;
	long lElapsedTime;
	struct timeval ts;
	int i,j;
	int N_CPUs, offset = 0, filas, filasXthread, resto;
	pthread_t * tid;
    thread_args * argumentos;

	do{
		printf("Ingrese la cantidad de nucleos de su procesador: ");
		fflush(stdin);
		scanf("%d", &N_CPUs);
	} while(N_CPUs <= 0);

	tid = malloc(N_CPUs*sizeof(pthread_t));
	argumentos = malloc(N_CPUs*sizeof(thread_args));

	filasXthread = SIZE/N_CPUs;
	resto = SIZE%N_CPUs;
	for(i=0; i<N_CPUs; i++){
		printf("inicio = %d -> ", offset);
		argumentos[i].offset = offset;
		filas = filasXthread;
		if(resto != 0){
			filas++;
			resto--;
		}
		printf("fin = %d\n", filas + offset - 1);
		argumentos[i].filas = filas;
		offset += filas;
	}

	// Inicializa la matriz con números al azar
	initmat(mat);
	
	gettimeofday(&ts, NULL);
	start_ts = ts.tv_sec; // Tiempo inicial

	// Eliminar de la matriz todos los números que no son primos
	// Esta es la parte que hay que paralelizar
	
	//for(i=0;i<SIZE;i++)
	//	for(j=0;j<SIZE;j++)
	//		if(!isprime(mat[i][j]))
	//		   mat[i][j]=0;
	
	for(i=0;i<N_CPUs;i++)
		pthread_create(&tid[i],NULL,tfunc,&argumentos[i]);
    
    for(i=0;i<N_CPUs;i++)
        pthread_join(tid[i],NULL);
	
	// Hasta aquí termina lo que se tiene que hacer en paralelo
	gettimeofday(&ts, NULL);
	stop_ts = ts.tv_sec; // Tiempo final
	elapsed_time = stop_ts - start_ts;


	printnonzeroes(mat);
	printf("------------------------------\n");
	printf("TIEMPO TOTAL, %lld segundos\n",elapsed_time);
}

void initmat(int mat[][SIZE])
{
	int i,j;
	
	srand(getpid());
	
	for(i=0;i<SIZE;i++)
		for(j=0;j<SIZE;j++)
			mat[i][j]=INICIAL+rand()%(FINAL-INICIAL);
}

void printnonzeroes(int mat[SIZE][SIZE])
{
	int i,j,count = 0;
	char opcion;
	printf("Desea imprimir los primos encontrados? ");
	fflush(stdin);
	scanf(" %c", &opcion);
	for(i=0;i<SIZE;i++)
		for(j=0;j<SIZE;j++)
			if(mat[i][j]!=0){
				if(opcion == 'y')
					printf("%d\n",mat[i][j]);
				count++;
			}
	printf("Se encontraron %d primos dentro la matriz\n", count);
}

			   
int isprime(int n)
{
	int d=3;
	int prime=n==2;
	int limit=sqrt(n);
	
	if(n>2 && n%2!=0)
	{
		while(d<=limit && n%d)
			d+=2;
		prime=d>limit;
	}
	return(prime);
}